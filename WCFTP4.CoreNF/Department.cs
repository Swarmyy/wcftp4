﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace WCFTP4.Core
{
    public class Department
    {
        public string Name { get; set; }

        public List<Promotion> DptPromotions { get; set; }

        public Department(string p_name, List<Promotion> p_dptPromotions = null)
        {
            Name = p_name ?? throw new ArgumentException("Le nom doit exister");
            DptPromotions = p_dptPromotions;
        }

        public List<Student> SearchFor(string p_studentName, string p_prom, int p_studentAge = 0)
        {
            var v_rtn = new List<Student>();
            var v_liststudentsName = new List<string>();
            foreach (var v_prom in DptPromotions)
            {
                if (p_prom!=null&&v_prom.Name == p_prom)
                {
                    foreach (var v_student in v_prom.ListStudents)
                    {
                        if ((v_student != null && v_student.Name.Contains(p_studentName)) && v_student.Age == p_studentAge)
                        {
                            v_rtn.Add(v_student);
                        }
                    }
                }
            }
            return v_rtn;
        }
    }
}