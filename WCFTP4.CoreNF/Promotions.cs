﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WCFTP4.Core
{
    public class Promotion
    {
        public List<Student> ListStudents { get; set; }

        public string Name { get; set; }

        public Promotion(string p_name, List<Student> p_listStudents = null)
        {
            ListStudents = p_listStudents;
            Name = p_name ?? throw new ArgumentException("Le nom doit exister");
        }
    }
}