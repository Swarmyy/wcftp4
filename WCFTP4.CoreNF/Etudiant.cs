﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WCFTP4.Core
{
    public class Student
    { 
        public string Name { get; set; }

        public string Surname { get; set; }

        public System.DateTime DateOfBirth { get; private set; }

        public int Age
        {
            get
            {
                var today = DateTime.Today;
                var age = today.Year - DateOfBirth.Year;
                return age;
            }
        }

        public Student(string p_name, string p_surname, DateTime p_dateOfBirth)
        {
            Name = p_name ?? throw new ArgumentException("Le nom doit exister");
            Surname = p_surname ?? throw new ArgumentException("Le prenom doit exister");
            DateOfBirth = DateOfBirth != null ? DateOfBirth : throw new ArgumentException("La date de naissance doit exister");
        }

    }
}