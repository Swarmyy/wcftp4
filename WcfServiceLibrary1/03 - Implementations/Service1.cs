﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using WcfServiceLibrary1.DataContracts;
using WcfServiceLibrary1.Services;
using WCFTP4.Core;

namespace WcfServiceLibrary1.Implementations
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "Service1" in both code and config file together.
    public class Service1 : IService1
    {
        List<Department> Depts;
        List<Promotion> Promos;
        
        public CompositeType GetDataUsingDataContract(CompositeType composite)
        {
            if (composite == null)
            {
                throw new ArgumentNullException("composite");
            }
            if (composite.BoolValue)
            {
                composite.StringValue += "Suffix";
            }
            return composite;
        }

        public void NewDept(string dept)
        {
            Department department = new Department(dept);
            Depts.Add(department);
        }

        public void NewPromotion(string promotion)
        {
            Promotion promo = new Promotion(promotion);
            Promos.Add(promo);
        }

        public void NewStudent(string name, string surname, DateTime dateofbirth, string promotion, string dept)
        {
            Student student = new Student(name, surname, dateofbirth);

        }

        public void ResearchStudent(string m_studentName, int m_studentAge, string m_departmentName, string m_promName)
        {
            Department a = new Department(m_departmentName);
            a.SearchFor(m_studentName, m_promName);
        }

    }
}
