﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using WcfServiceLibrary1.DataContracts;

namespace WcfServiceLibrary1.Services
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IService1" in both code and config file together.
    [ServiceContract]
    public interface IService1
    {

        [OperationContract]
        CompositeType GetDataUsingDataContract(CompositeType composite);

        [OperationContract]
        void NewStudent(string name, string surname, DateTime dateofbirth, string promotion, string dept);

        [OperationContract]
        void NewPromotion(string promotion);

        [OperationContract]
        void NewDept(string dept);

        [OperationContract]
        void ResearchStudent(string m_studentName, int m_studentAge, string m_departmentName, string m_promName);

        // TODO: Add your service operations here
    }
}
